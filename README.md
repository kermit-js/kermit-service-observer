[![Kermit Module](https://i.imgur.com/DY1puas.png)](https://www.npmjs.com/package/kermit)

# kermit-service-observer - 0.1.1

Inter service event listener for [kermit](https://www.npmjs.com/package/kermit).

## Install

`$ npm install --save kermit-service-observer`

## Include

Register the observer service in your application services config (eg. `config/application.config.js`).

```javascript
module.exports = {
  ...
  app: {
    services: {
      observer: 'kermit-service-observer/ObserverService'
      ...
    }
  }
  ...
}
```

## Use

A common use case is to execute an panic method in case of any application
service triggers an error, for example in case of a lost db connection etc.

To archive this register the listener in your application's pre launch phase:

```javascript
  // src/Application.js
  ...
  
  launch() {
    super.launch();
    
    let observer = this.getServiceManager().get('observer');
    
    // execute the callback in case of any service (referenced by its service-key)
    // triggers an "error" event.
    observer.awaitAny([
      'mongo-service', 'redis-service', 'rabbit-service'
    ], 'error', (err, observation) => {
        if (err) {
          // an error within the service observation occured.
          throw err;
        }
        
        // this is where your recovery code goes.
        
        console.error(
          'the service', observation.service,
          `triggered an ${observation.event} event, passing the following arguments:`,
          observation.args
        );
    });
    
    return this;
  }
  ...
  
```

The service observer is also handy to use for assure the readyness of your
asynchronous application services:

```javascript
  // src/Application.js
  ...
  
  launch() {
    super.launch();
    
    let observer = this.getServiceManager().get('observer');
    
    // execute the callback in case of all services (referenced by its service-keys)
    // have triggered an "ready" event.
    observer.awaitAll([
      'mongo-service', 'redis-service', 'rabbit-service'
    ], 'ready', (err, observations) => {
      if (err) {
        // an error within the service observation occured.
        throw err;
      }
      
      // this is where your on-all-services-ready-to-use code goes.
      console.info('All services are ready!');
    });
    
    return this;
  }
  ...
```

### LICENSE

The files in this archive are released under BSD-2-Clause license.
You can find a copy of this license in [LICENSE](https://gitlab.com/kermit-js/kermit-service-observer/raw/master/LICENSE).
/**
 * kermit - soa infrastructure for node js
 *
 * @copyright   Copyright (c) 2016, Alrik Zachert
 * @license     https://gitlab.com/kermit-js/kermit-service-observer/blob/master/LICENSE BSD-2-Clause
 */

'use strict';

const Service = require('kermit/Service');

/**
 * The inter service event observer class.
 */
class ObserverService extends Service {
  /**
   * Observe the list of services for triggering the given event.
   *
   * @param {Array|String} services
   * @param {String} event
   * @param {Object} [options]
   * @param {Boolean} [options.waitForAll] - The callback will be fired as soon as all service events have been triggered.
   * @param {Boolean} [options.once] -  The callback will be executed only once for each service, or in combination
   *                                    with waitForAll, once for the whole list.
   * @param {Function} callback
   * @returns {ObserverService}
   */
  observe(services, event, options, callback) {
    let
      sm = this.getServiceManager(),
      queue = [],
      listenMethod;

    if (typeof services === 'string') {
      services = [services];
    }

    if (arguments.length === 3) {
      callback = options;
      options = {};
    }

    listenMethod = (options.once ? 'once' : 'on');

    for (let watchPoint of services) {
      if (typeof watchPoint === 'string') {
        watchPoint = sm.get(watchPoint, true);
      }

      if (typeof watchPoint[listenMethod] !== 'function') {
        throw new Error('Trying to observe a non EventEmitter.');
      }

      if (options.waitForAll) {
        queue.push(watchPoint);
      }

      this._observeWatchPoint(
        watchPoint, listenMethod, event,
        queue, options.waitForAll, callback
      );
    }

    return this;
  }

  /**
   * Register the event listener on the given service (watchPoint) and if requested wait for the whole queue to finish.
   *
   * @param {kermit.Service} watchPoint
   * @param {String} listenMethod
   * @param {String} event
   * @param {kermit.Service[]} queue
   * @param {Boolean} waitForAll
   * @param {Function} callback
   * @private
   */
  _observeWatchPoint(watchPoint, listenMethod, event, queue, waitForAll, callback) {
    watchPoint[listenMethod](event, (...args) => {
      if (waitForAll) {
        this._tryResolveQueue(queue, watchPoint, event, args, callback);
      } else {
        callback(null, {
          service: watchPoint,
          event: event,
          args: args
        });
      }
    });
  }

  /**
   * Cache the observations of all resolved watch points and resolve the whole queue by executing the callback
   * as soon as all watch points of the queue have fired their event.
   *
   * @param {kermit.Service[]} queue
   * @param {kermit.Service} watchPoint
   * @param {String} event
   * @param {Array} args
   * @param {Function} callback
   * @private
   */
  _tryResolveQueue(queue, watchPoint, event, args, callback) {
    let
      index = queue.indexOf(watchPoint),
      observations = queue._observations || (queue._observations = []);

    if (index > -1) {
      queue.splice(index, 1);

      observations.push({
        service: watchPoint,
        event: event,
        args: args
      });
    }

    if (queue.length === 0) {
      callback(null, observations);
    }
  }

  /**
   * Execute the given callback as soon as any service has triggered the given event.
   * The callback may be called multiple times, even for the same service.
   *
   * @param {String[]} services
   * @param {String} event
   * @param {Function} callback
   * @returns {ObserverService}
   */
  awaitAny(services, event, callback) {
    return this.observe(services, event, callback);
  }

  /**
   * Execute the given callback, as soon as all services have triggered the given event.
   * The callback will only be called once or never.
   *
   * @param {String[]} services
   * @param {String} event
   * @param {Function} callback
   * @returns {ObserverService}
   */
  awaitAll(services, event, callback) {
    return this.observe(services, event, {
      waitForAll: true,
      once: true
    }, callback);
  }
}

module.exports = ObserverService;
